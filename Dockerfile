FROM node:14-alpine

WORKDIR /app

COPY package.json ./

RUN yarn

COPY . .

EXPOSE 8000

RUN yarn global add nodemon

CMD ["yarn", "start"]
