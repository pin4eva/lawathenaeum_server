import express from "express";
import RavePay from "flutterwave-node-v3";

import config from "../config";
const router = express.Router();
const rave = new RavePay(
  config.flwPK,
  config.flwSK,
  process.env.NODE_ENV === "production"
);

const payload = {
  card_number: "5531886652142950",
  cvv: "564",
  expiry_month: "09",
  expiry_year: "21",
  currency: "NGN",
  amount: "100",
  redirect_url: "https://www.google.com",
  fullname: "Olufemi Obafunmiso",
  email: "olufemi@flw.com",
  phone_number: "0902620185",
  enckey: config.flwEK,
  //   tx_ref: "MC-32444ee--4eerye4euee3rerds4423e43e", // This is a unique reference, unique to the particular transaction being carried out. It is generated when it is not provided by the merchant for every transaction.
};
router.post("/payment", async (req, res) => {
  try {
    const response = await rave.Charge.card(payload);
    console.log(response);
    if (response.meta.authorization.mode === "pin") {
      let payload2 = payload;
      payload2.authorization = {
        mode: "pin",
        fields: ["pin"],
        pin: 3310,
      };
      const reCallCharge = await rave.Charge.card(payload2);

      const callValidate = await rave.Charge.validate({
        otp: "12345",
        flw_ref: reCallCharge.data.flw_ref,
      });
      console.log(callValidate);
    }
    if (response.meta.authorization.mode === "redirect") {
      var url = response.meta.authorization.redirect;
      open(url);
    }
  } catch (error) {
    res.json(error);
  }
});

export default router;
