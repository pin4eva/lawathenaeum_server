export const seedUsers = [
  {
    _id: "5ebc79539e4223000847d6b4",
    role: "user",
    isActive: false,
    type: "Free",
    name: "Peter Hotmail",
    email: "pin4eva@hotmail.com",
    phone: "07060668092",
    password: "$2a$10$ozEo9XYdhwejTHxjGU8kf.ZN1HOvDPqQ2Jz/wFsrIDp3DiG5NZPxu",

    bio: "Full stack software engineer at Law Athenaeum ",
    contact: "112 Item road Aba",
    image: "https://edfhr.s3.amazonaws.com/user/IMG-20190317-WA0014.jpg",
  },
  {
    _id: "5eee1488468412484898ea6a",
    role: "user",
    isActive: false,

    name: "Peter Gmail",
    email: "pin4eva@gmail.com",
    password: "$2a$10$kXQon6P5rSNXsVAsInVhBO.4wnllCe3NfVYsBVjma/clsdLuYhYZi",
    plan: "Student",

    image: "https://edfhr.s3.amazonaws.com/user/_MG_8358.jpg",
    bio: null,
    contact: null,
    phone: null,
  },
  {
    _id: "5eee1489468412484898ea6b",
    role: "editor",
    isActive: false,

    name: "Peter Akaliro",
    email: "pin4eva@gmail.com",
    password: "$2a$10$aLoclAwj59JibQg2.tB.KejIvU84CSEUk9eshphY0ot60dKgOWQUa",
    plan: "Student",
  },
];
