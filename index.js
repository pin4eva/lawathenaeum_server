import { ApolloServer } from "apollo-server-express";
import cors from "cors";
import express from "express";
import { connectDB } from "./db";
import { schema } from "./graphql/typeDefs";
// import payment from "./lib/payment";

const app = express();

// middlewares
app.use(cors());
app.use(express.json());
app.use(express.urlencoded({ extended: true }));

// app.use("/api/rest", payment);

const server = new ApolloServer({
  schema,
  context: async ({ req, res }) => {
    const { authorization } = req.headers;

    return {
      res,
      req,
      token: authorization,
    };
  },
  playground: process.env.NODE_ENV !== "production",
});

server.applyMiddleware({ app, path: "/api/graphql" });

app.get("/", (req, res) => {
  res.send("Home");
});

const Port = process.env.PORT || 8000;

const start = async () => {
  const db = await connectDB();

  if (db) {
    app.listen(Port, (err) => {
      if (err) return process.exit(1);
      console.log(`server started on ${Port}`);
    });
  } else {
    process.exit(1);
  }
};

start();

// export default server;
