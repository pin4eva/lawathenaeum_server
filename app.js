import express from "express";
import { ApolloServer } from "apollo-server-express";
import { schema } from "./graphql/typeDefs";

const app = express();

// app.get("/", (req, res) => {
//   res.status(200).send("Welcome home");
// });

const apolloServer = new ApolloServer({
  schema,
  context: async ({ req, res }) => {
    const { authorization } = req.headers;

    return {
      res,
      req,
      token: authorization,
    };
  },
});

apolloServer.applyMiddleware({ app, path: "/" });

export default app;
